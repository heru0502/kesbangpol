Panduan installasi Aplikasi Kesbangpol

- pada direktori web jalankan perintah "git clone https://gitlab.com/heru0502/kesbangpol.git"
- rename file **application/config/config.php.example** menjadi **application/config/config.php**
- rename file **application/config/database.php.example** menjadi **application/config/database.php**
- pada file **application/config/config.php** ubah **http://your-site.com** dengan domain website
- pada file **application/config/database.php** sesuaikan username, password, dan nama database
- pada root direktori jalankan perintah **composer update**
- pada root direktori jalankan perintah **php index.php luthier migrate**