<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function notification($group)
{
    $ci =& get_instance();    
    $user = $ci->ion_auth->user()->row();
    $ci->load->model('member/Submission');

    if ($group == 'member')
    {
        $data['total_all'] = Submission::where('created_by', $user->id)
                                        ->whereIn('submission_status', array(-1, 2, 3))
                                        ->count();
        $data['total_numbered'] = Submission::where('created_by', $user->id)
                                            ->where('submission_status', 2)
                                            ->count();
        $data['total_signatured'] = Submission::where('created_by', $user->id)
                                            ->where('submission_status', 3)
                                            ->count();
        $data['total_revision'] = Submission::where('created_by', $user->id)
                                            ->where('submission_status', -1)
                                            ->count();
    }
    else
    {
        $data['total_all'] = Submission::whereIn('submission_status', array(1, 2))->count();
        $data['total_progress'] = Submission::where('submission_status', 1)->count();
        $data['total_numbered'] = Submission::where('submission_status', 2)->count();
    }
    
    return $data;
}