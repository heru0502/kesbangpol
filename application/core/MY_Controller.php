<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct()
    {

        parent::__construct();

        date_default_timezone_set("Asia/Makassar");

    }

}

/* Hak Akses user */
class User_Controller extends MY_Controller {

    function __construct()
    {
        parent::__construct();

        // Cek Session
        // if(!$this->session->userdata('kesbangpol_logged')){
        //     redirect('admin/login', 'refresh');
        // }    

        // // Get value session
        // $this->varSess = $this->session->userdata('eoffice_user_logged'); 
        
        // $this->role = 'user';        
        // $this->btn = $this->input->post('submit');
        // $this->load->library('TemplateUser', NULL, 'render');
        
        // // Get User Detail
        // $this->load->model("$this->role/Md_Auth","modelAuth");
        // $this->userDetail = $this->modelAuth->getUserDetail($this->varSess['id_system'])->row();
    }
    

}

/* Hak Akses User */
class Admin_Controller extends MY_Controller {

    function __construct()
    {
        parent::__construct();        
		$this->load->library('ion_auth');

        // Cek Session
        // if(!$this->session->userdata('kesbangpol_logged')){
        //     redirect('admin/login', 'refresh');
        // }   
        if (!$this->ion_auth->logged_in())
        {
            redirect('admin/auth/login');
        }

        // $this->varSession = $this->session->userdata('kesbangpol_logged');  
    }

    function _timestampUser($type)
    {
        if ($type === 'created') {
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['created_by'] = $this->varSession['id'];
        } else {
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['updated_by'] = $this->varSession['id'];
        }

        return $data;
    }

    function generateDataIndex(){
		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw = $_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length = $_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start = $_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search = $_REQUEST['search']["value"];

		/*Menghitung total desa didalam database*/
        $total = $this->db->count_all_results($this->table);

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output = array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw'] = $draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal'] = $output['recordsFiltered'] = $total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data'] = array();

		/*Lanjutkan pencarian ke database*/
        $query = $this->mod->getDataTables($search, $length, $start, $count = false);

		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search != ""){
            $jum = $this->mod->getDataTables($search, $length, $start, $count = true);
            $output['recordsTotal'] = $output['recordsFiltered'] = $jum->num_rows();
		}

		$nomor_urut = $start+1;
		foreach ($query->result_array() as $key => $value) {

            $output['data'][$key]['no'] = $nomor_urut;
            foreach ($this->columnShows as $columnShow) {
                $output['data'][$key][$columnShow] = $value[$columnShow];
            }
		    $nomor_urut++;
        }

		echo json_encode($output);
	}

}