<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminMiddleware implements Luthier\MiddlewareInterface
{

    /**
     * Middleware entry point
     *
     * @return void
     */
    public function run($args)
    {
        if (!ci()->ion_auth->logged_in()) {
            redirect('admin/login', 'refresh');
        }

        if (!ci()->ion_auth->in_group($args)) {
            show_404();
        }
        
    }
}