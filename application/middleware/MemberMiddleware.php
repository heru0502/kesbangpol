<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MemberMiddleware implements Luthier\MiddlewareInterface
{

    /**
     * Middleware entry point
     *
     * @return void
     */
    public function run($args = [])
    {
        if (!ci()->ion_auth->logged_in()) {
            redirect(route('member.login'), 'refresh');
        }

        if (!ci()->ion_auth->in_group($args)) {
            show_404();
        }
    }
}