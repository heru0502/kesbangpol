<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_menimbang_to_submissions_table extends CI_Migration
{
    public function up()
    {
        $fields = array(
            'menimbang' => array(
                'type' => 'VARCHAR',
                'constraint' => 500,
                'null' => true,
                'after' => 'revision_note'
            )
        );
        $this->dbforge->add_column('submissions', $fields);
    }

    public function down()
    {

    }
}