<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_users_groups_table extends CI_Migration
{
    protected $table = 'users_groups';

    public function up()
    {
        $this->dbforge->add_field(array(
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ),
            'group_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8,
                'unsigned' => TRUE,
            ),
        ));
        $this->dbforge->add_key(array('user_id', 'group_id'), TRUE);
        
        $this->dbforge->create_table($this->table);

        $this->load->helper('db');
        $this->db->query(add_foreign_key($this->table, 'user_id', 'users(id)', 'CASCADE', 'NO ACTION'));
        $this->db->query(add_foreign_key($this->table, 'group_id', 'groups(id)', 'CASCADE', 'NO ACTION'));

        /* Seeding */
        $this->db->insert_batch($this->table, $this->seedData());
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }

    private function seedData() 
    {
        $seedData = array(
            array(
                'user_id' => 1,
                'group_id' => 1,
            ),
            array(
                'user_id' => 2,
                'group_id' => 1,
            ),
        );
        return $seedData;
    }
}