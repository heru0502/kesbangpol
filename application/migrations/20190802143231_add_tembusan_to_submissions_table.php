<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_tembusan_to_submissions_table extends CI_Migration
{
    public function up()
    {
        $fields = array(
            'tembusan' => array(
                'type' => 'TEXT',
                'null' => true,
                'after' => 'tanggal_surat'
            )
        );
        $this->dbforge->add_column('submissions', $fields);
    }

    public function down()
    {

    }
}