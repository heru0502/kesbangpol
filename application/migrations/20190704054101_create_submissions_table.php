<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_submissions_table extends CI_Migration
{
    protected $table = 'submissions';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'submission_status' => array(
                'type' => 'TINYINT',
                'constraint' => 3,
                'default' => 0,
            ),
            'identity_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'address' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'institution' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'faculty' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'research_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => TRUE,
            ),
            'research_title' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'research_location' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'research_time_start' => array(
                'type' => 'DATE',
            ),
            'research_time_end' => array(
                'type' => 'DATE',
            ),
            'research_category' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'research_status' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'attachment' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'revision_note' => array(
                'type' => 'TEXT',
                'null' => TRUE,
            ),
            'created_at' => array(
                'type' => 'TIMESTAMP',
            ),
            'updated_at' => array(
                'type' => 'TIMESTAMP',
                'null' => TRUE,
            ),
            'checked_at' => array(
                'type' => 'TIMESTAMP',
                'null' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
            'checked_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
        ));
        $this->dbforge->add_key('id', TRUE);        
        $this->dbforge->create_table($this->table);

        $this->load->helper('db');
        $this->db->query(add_foreign_key($this->table, 'created_by', 'users(id)', 'RESTRICT', 'RESTRICT'));
        $this->db->query(add_foreign_key($this->table, 'updated_by', 'users(id)', 'RESTRICT', 'RESTRICT'));
        $this->db->query(add_foreign_key($this->table, 'checked_by', 'users(id)', 'RESTRICT', 'RESTRICT'));
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }
}