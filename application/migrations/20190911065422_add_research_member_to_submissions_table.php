<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_research_member_to_submissions_table extends CI_Migration
{
    public function up()
    {
        $fields = array(
            'research_member' => array(
                'type' => 'TEXT',
                'null' => true,
                'after' => 'research_status',
            )
        );
        $this->dbforge->add_column('submissions', $fields);
    }

    public function down()
    {

    }
}