<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_picture_to_users_table extends CI_Migration
{
    public function up()
    {
        $fields = array(
            'picture' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true,
                'after' => 'name'
            )
        );
        $this->dbforge->add_column('users', $fields);
    }

    public function down()
    {

    }
}