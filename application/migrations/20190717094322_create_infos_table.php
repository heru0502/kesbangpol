<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_infos_table extends CI_Migration
{

    protected $table = 'infos';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
            ),
            'info' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->table);

        /* Seeding */
        $this->db->insert_batch($this->table, $this->seedData());
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }

    private function seedData() 
    {
        $seedData = array(
            array(
                'id' => 'attachment',
                'info' => '<!DOCTYPE html><html><head></head><body><p>A. PERSYARATAN:</p><ol><li>DARI MASYARAKAT INDIVIDU, SURAT PENGANTAR KADES/LURAH SESUAI TEMPAT TINGGAL/DOMISILI</li><li>MAHASISWA PELAJAR SURAT PENGANTAR DARI LEMBAGA PENDIDIKAN/PERGURUAN TINGGI</li><li>BADAN USAHA SURAT PENGANTAR DARI PIMPINAN BADAN USAHA DAN COPY AKTA NOTARIS PENDIRIAN</li><li>APARATUR PEMERINTAH SURAT PENGATUR DARI PIMPINAN ORGANISASI KEMASYARAKATAN DAN AKTA NOTARIS PENDIRIAN</li><li>ORGANISASI NIRLABA LAINNYA SURAT PENGANTAR DARI PIMPINAN ORGANISASI NIRLABA DAN COPY AKTA NOTARIS PENDIRIAN DAN DILAMPIRKAN :<br /><ol style="list-style-type: upper-alpha;"><li>PROPOSAL YANG BERISIKAN&nbsp;<br /><ul><li>LATAR BELAKANG</li><li>MAKSUD DAN TUJUAN</li><li>RUANG LINGKUP</li><li>JANGKA WAKTU PENELITIAN</li><li>NAMA PENELITIAN</li><li>SASARAN / TARGET PENELITIAN</li><li>METODE PENELITIAN</li><li>LOKASI PENELITIAN</li><li>HASIL YANG DIHARAPKAN DARI PENELITI</li><li>PENELITI LEBIH DARI DATU (DIBUAT DAFTAR SESUAI LOKASI PENELITIAN)</li><li>PROPOSAL DIMASUKAN DALAM MAP DILENGKAPI NAMA, ALAMAT, NO HP</li></ul></li><li>SALINAN / FOTO COPY KARTU PENDUDUK DAN KARTU MAHASISWA (PERORANGAN)</li><li>SALINAN / FOTO COPY KARTU PENDUDUK LEBIH DARI SATU / PENANGGUNG JAWAB / KETUA / KOORDINATOR</li><li>PAS FOTO 3X4&nbsp;(BERWARNA)</li><li>SURAT PERNYATAAN UNTUK MENATAATI DAN TIDAK MELANGGAR KETENTUAN PERATURAN PERUNDANG-UNDANGAN YANG BERLAKU</li></ol></li></ol></body></html>',
            ),
        );
        return $seedData;
    }
}