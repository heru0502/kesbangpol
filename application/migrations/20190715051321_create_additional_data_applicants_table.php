<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_additional_data_applicants_table extends CI_Migration
{

    protected $table = 'additional_data_applicants';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ),
            'identity_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'address' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'institution' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'faculty' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->table);

        $this->load->helper('db');
        $this->db->query(add_foreign_key($this->table, 'user_id', 'users(id)', 'CASCADE', 'CASCADE'));
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }
}