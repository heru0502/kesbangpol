<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_users_table extends CI_Migration
{

    protected $table = 'users';
    
    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'ip_address' => array(
                'type' => 'VARCHAR',
                'constraint' => 45
            ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => TRUE,
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'unique' => TRUE,
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
                'null' => TRUE,
            ),
            'activation_selector' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'unique' => TRUE,
                'null' => TRUE,
            ),
            'activation_code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => TRUE,
            ),
            'forgotten_password_selector' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'unique' => TRUE,
                'null' => TRUE,
            ),
            'forgotten_password_code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => TRUE,
            ),
            'forgotten_password_time' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
            'remember_selector' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'unique' => TRUE,
                'null' => TRUE,
            ),
            'remember_code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => TRUE,
            ),
            'created_on' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ),
            'created_at' => array(
                'type' => 'TIMESTAMP',
            ),
            'updated_at' => array(
                'type' => 'TIMESTAMP',
                'null' => TRUE,
            ),
            'last_login' => array(
                'type' => 'TIMESTAMP',
                'null' => TRUE,
            ),
            'active' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->table);

        /* Seeding */
        $this->db->insert_batch($this->table, $this->seedData());
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }

    private function seedData() 
    {
        $seedData = array(
            array(
                'ip_address' => 'admin',
                'username' => 'heru0502',
                'password' => password_hash("rahasia", PASSWORD_BCRYPT),
                'email' => 'heru0502@gmail.com',
                'name' => 'Heru Firmansyah',
                'active' => 1,
            ),
            array(
                'ip_address' => 'admin',
                'username' => 'Administrator',
                'password' => password_hash("rahasia", PASSWORD_BCRYPT),
                'email' => 'bakesbangpol@banjarkab.go.id',
                'name' => 'Administrator',
                'active' => 1,
            ),
        );
        return $seedData;
    }
}