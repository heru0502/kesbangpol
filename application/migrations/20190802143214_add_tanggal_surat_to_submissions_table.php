<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_tanggal_surat_to_submissions_table extends CI_Migration
{
    public function up()
    {
        $fields = array(
            'tanggal_surat' => array(
                'type' => 'DATE',
                'null' => true,
                'after' => 'menimbang'
            )
        );
        $this->dbforge->add_column('submissions', $fields);
    }

    public function down()
    {

    }
}