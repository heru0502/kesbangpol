<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_login_attempts_table extends CI_Migration
{
    protected $table = 'login_attempts';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'ip_address' => array(
                'type' => 'VARCHAR',
                'constraint' => 45,
            ),
            'login' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
            ),
            'time' => array(
                'type' => 'TIMESTAMP',
            ),
        ));
        $this->dbforge->add_key('id', TRUE);        
        $this->dbforge->create_table($this->table);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }
}