<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_groups_table extends CI_Migration
{
    protected $table = 'groups';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 20,
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->table);

        /* Seeding */
        $this->db->insert_batch($this->table, $this->seedData());
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }

    private function seedData() 
    {
        $seedData = array(
            array(
                'name' => 'admin',
                'description' => 'Administrator',
            ),
            array(
                'name' => 'employee',
                'description' => 'Employee',
            ),
            array(
                'name' => 'applicant',
                'description' => 'Applicant',
            ),
        );
        return $seedData;
    }
}