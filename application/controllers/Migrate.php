<?php

class Migrate extends CI_Controller
{

    public function index()
    {
        $this->load->library('migration');

        foreach (range(1, 5) as $number) {
            if ($this->migration->version($number) === FALSE) {
                show_error($this->migration->error_string());
            }
        }                
    }

}
