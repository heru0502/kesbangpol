<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DataTableController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->table = 'users';
        // $this->columnShows = array('id','email','name','username');
        // $this->columnSearches = array('email','name');
        // $this->load->model('DataTable');
    }

    
    /**
     * Index action
     */
    public function index()
    {

    }

    public function test()
    {
        echo 'halo';
    }

    public function generateDataIndex(){
        /*Menagkap semua data yang dikirimkan oleh client*/
        $table = new \stdClass;
        $search = new \stdClass;
        $modelName = $this->input->post('modelName');
        $modelLocation = $this->input->post('modelLocation');
        $table->columnSelects = $this->input->post('columnSelects');        
        $search->columns = $this->input->post('columnSearches');

        $this->load->model($modelLocation);
        $dataTable = new $modelName;
        $query = $dataTable->index();

        /* date range */
        $dateRange = $this->input->post('dateRange');// (array_key_exists('dateRange', $_REQUEST) ? $_REQUEST['dateRange']['value'] : '');

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw = $_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length = $_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start = $_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search->keyword = $_REQUEST['search']["value"];

		/*Menghitung total desa didalam database*/
        // $total = $this->db->count_all_results($table['name']);
        $total = $query->count();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output = array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw'] = $draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal'] = $output['recordsFiltered'] = $total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data'] = array();

		/*Lanjutkan pencarian ke database*/
        // $query = $this->mod->getDataTables($table, $search, $length, $start, $count = false);
        if($search->keyword != ""){
            $query->where(function($q) use($search) {
                foreach ($search->columns as $key => $column) {
                    if ($key < 1) {
                        $q->where($column, 'like', "%$search->keyword%");
                    } else {
                        $q->orWhere($column, 'like', "%$search->keyword%");
                    }
                }                
            });
        }

        if ($dateRange != '') {
            $dateRange = explode(' - ', $dateRange);
            $from = date('Y-m-d', strtotime(str_replace('/', '-', $dateRange[0])));
            $now = date('Y-m-d', strtotime(str_replace('/', '-', $dateRange[1]) . ' +1 day'));
            $query->whereBetween('created_at', [$from, $now]);
        }

		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search->keyword != ""){
            $output['recordsTotal'] = $output['recordsFiltered'] = $query->count();
		}
        
        $query->orderBy('id', 'desc');
        if ($length >= 0) {
            $query->skip($start)->take($length);
        } 
        else {
            if ($dateRange == '')
                $query->take(0);
        }
        $result = $query->get();

        $nomor_urut = $start+1;
		foreach ($result as $key => $value) {
            $output['data'][$key]['no'] = $nomor_urut;
            foreach ($table->columnSelects as $columnShow) {
                $output['data'][$key][$columnShow] = $value[$columnShow];
            }
		    $nomor_urut++;
        }

		echo json_encode($output);
	}

}