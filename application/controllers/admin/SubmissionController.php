<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SubmissionController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();  
        $this->load->model('admin/Submission');
        $this->user = $this->ion_auth->user()->row();

        /* Set Groups Allowed */
        $this->middlewarefilter->filter('AdminMiddleware', ['admin','employee']);
    }

    /**
     * Index action
     */
    public function index()
    {
        $this->twig->display('admin/submission/index');
    }

    public function update($id)
    {
        $data['current_data'] = Submission::find($id)->toArray();
        $data['form_type'] = 'update';
        $this->twig->display('admin/submission/update', $data);
    }

    public function revision($id)
    {
        $data['current_data'] = Submission::find($id)->toArray();
        $data['form_type'] = 'revision';
        $this->twig->display('admin/submission/update', $data);
    }

    public function numbering($id)
    {
        $data['current_data'] = Submission::find($id)->toArray();
        $data['form_type'] = 'numbering';
        $this->twig->display('admin/submission/update', $data);
    }

    public function delete($id)
    {
        Submission::destroy($id);
        redirect(route('admin.submission'), 'refresh');
    }

    public function printSubmission($id)
    {
        $this->load->library('PdfGenerator');
        $data['current_data'] = Submission::find($id)->toArray();
        $html = $this->twig->render('admin/submission/print', $data, true);
		$fileName = 'Ijin_Penelitian';			
		$this->pdfgenerator->generate($html, $fileName, 'folio', 'portrait');
    }

    public function store()
    {      
        $form_type = $this->input->post('form_type'); 
        $this->form_validation->set_rules($this->_configValidation($form_type));         
        $dataForm = (object) $this->input->post();

        if ($this->form_validation->run()) 
        {            
            $research = Submission::find($dataForm->id);
            $submission = Submission::find($dataForm->id);
            $submission->checked_at = date('Y-m-d H:i:s');
            $submission->checked_by = $this->user->id;
            if ($form_type == 'update') 
            {
                $submission->submission_status = 3;
                
                $subject = 'KESBANGPOL - Selesai';
                $message = "Pengajuan penelitian anda dengan judul $research->research_title telah selesai ditandatangani dan siap diambil";
            } 
            else if ($form_type == 'revision')
            {
                $submission->submission_status = -1;
                $submission->revision_note = $dataForm->revision_note;

                $subject = 'KESBANGPOL - Revisi';
                $message = "Pengajuan penelitian anda dengan judul $research->research_title memerlukan revisi";
            }
            else if ($form_type == 'numbering')
            {
                $submission->submission_status = 2;
                $submission->research_number = $this->_researchNumberGenerate();
                $submission->menimbang = $dataForm->menimbang;
                $submission->tanggal_surat = date('Y-m-d', strtotime($dataForm->tanggal_surat));
                $submission->tembusan = $dataForm->tembusan;

                $subject = 'KESBANGPOL - Syarat Lengkap';
                $message = "Pengajuan penelitian anda dengan judul $research->research_title telah memenuhi persyartan dan sedang menunggu ditanda tangani";
            }    
            
            $submission->save();
            sendMail($this->user->email, $subject, $message);
            redirect(route('admin.submission'), 'refresh');
        }
        else
        {
            $form_type == 'revision' ? $this->revision($dataForm->id) : $this->numbering($dataForm->id);
        }    
    }

    private function _configValidation($form_type)
    {
        if ($form_type == 'revision') 
        {
            $config = array(
                array(
                    'field' => 'revision_note',
                    'label' => 'Catatan Revisi',
                    'rules' => 'required'
                ),
            );
        }
        else if ($form_type == 'numbering') 
        {
            $config = array(
                array(
                    'field' => 'menimbang',
                    'label' => 'Menimbang',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'tanggal_surat',
                    'label' => 'Tanggal Surat Rekomendasi',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'tembusan',
                    'label' => 'Tembusan',
                    'rules' => 'required'
                ),
            );
        }
        else {
            $config = array(
                array(
                    'field' => 'buttonSubmit',
                    'rules' => 'required'
                ),
            );
        }
        return $config;
    }

    function _researchNumberGenerate()
    {
        $param = new stdClass();
        $param->digit = 3;
        $param->initial = ['070 /', '-II/KESBANGPOL/2019'];
        $submission = Submission::selectRaw('SUBSTRING(research_number, 6, 3) AS current_research_number')
                                ->whereRaw('SUBSTRING(research_number, 1, 5) =' . "'" .$param->initial[0]. "'")
                                ->whereRaw('SUBSTRING(research_number, 9, 19) =' . "'" .$param->initial[1]. "'")
                                ->whereRaw('LENGTH(research_number) = 27')
                                ->orderBy('research_number', 'desc')
                                ->first();

        if ($submission->count() > 0) {
            $new_research_number = $submission->current_research_number + 1;
        } else {
            $new_research_number = 1;
        }

        $research_number = $param->initial[0] . str_pad($new_research_number, $param->digit, '0', STR_PAD_LEFT) . $param->initial[1];
        return $research_number;
    }

}