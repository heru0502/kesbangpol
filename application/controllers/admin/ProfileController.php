<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/User');
        $this->user = $this->ion_auth->user()->row();
        $this->datetimeNow = date('YmdHis');
        $this->pictureName = null;

        /* Set Groups Allowed */
        $this->middlewarefilter->filter('AdminMiddleware', ['admin','employee']);
    }

    
    /**
     * Index action
     */
    public function index()
    {
        $data['user'] = $this->user;
        $data['current_data'] = User::find($this->user->id)->toArray();
        $this->twig->display('admin/profile/index', $data);
    }

    public function store()
    {
        $this->form_validation->set_rules($this->_configValidation());         
        $dataForm = (object) $this->input->post();

        if ($this->form_validation->run()) 
        {
            $user = User::find($this->user->id);
            $user->username = $dataForm->email;
            $user->email = $dataForm->email;
            $user->name = $dataForm->name;
            if (!empty($_FILES['picture']['name']))
                $user->picture = $this->pictureName;
            if ($dataForm->password != '') 
                $user->password = password_hash($dataForm->password, PASSWORD_BCRYPT);
            $user->save();

            redirect(route('admin.profile'), 'refresh');
        }
        else
        {
            $this->index();
        }
    }

    private function _configValidation()
    {
        $config = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required'
            ),
            array(
                'field' => 'name',
                'label' => 'Nama / Obyek',
                'rules' => 'required'
            ),
            array(
                'field' => 'picture',
                'rules' => (!empty($_FILES['picture']['name']) ? 'callback__rule_attachment' : ''),
            ),
        );
        return $config;
    }

    function _rule_attachment()
	{
		$config['upload_path']          = './uploads/profiles';
		$config['file_name']            = $this->datetimeNow;
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 5000;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('picture')){
			$messageError = $this->upload->display_errors();
            $this->form_validation->set_message('_rule_attachment', $messageError);
			return false;
		}
		else{		
            $upload_data = $this->upload->data(); 
            $this->pictureName = $upload_data['file_name'];
			return true;
		}
	}

}