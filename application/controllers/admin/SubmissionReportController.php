<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SubmissionReportController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/SubmissionReport');

        /* Set Groups Allowed */
        $this->middlewarefilter->filter('AdminMiddleware', ['admin','employee']);
    }

    
    /**
     * Index action
     */
    public function index()
    {
        $this->twig->display('admin/submission_report/index');
    }

    public function printSubmissionReport()
    {
        $this->load->library('PdfGenerator');
        $dateRange = $this->input->post('reservation');
        $dateRangeExplode = explode(' - ', $dateRange);
        $from = date('Y-m-d', strtotime(str_replace('/', '-', $dateRangeExplode[0])));
        $now = date('Y-m-d', strtotime(str_replace('/', '-', $dateRangeExplode[1]) . ' +1 day'));

        $submission = new SubmissionReport;
        $data['current_data'] = $submission->index()->whereBetween('created_at', [$from, $now])->get();
        $data['date_renge'] = $dateRange;
        $html = $this->twig->render('admin/submission_report/print', $data, true);
		$fileName = 'Laporan_Pengajuan_Penelitian';			
		$this->pdfgenerator->generate($html, $fileName, 'A4', 'landscape');
    }

}