<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Employee');

        /* Set Groups Allowed */
        $this->middlewarefilter->filter('AdminMiddleware', ['admin']);
    }
    
    /**
     * Index action
     */
    public function index()
    {
        $this->twig->display('admin/employee/index');
    }

    public function create()
    {
        $data['form_type'] = 'create';
        $this->twig->display('admin/employee/create', $data);
    }

    public function update($id)
    {
        $data['current_data'] = $this->ion_auth->user($id)->row();
        $data['form_type'] = 'update';
        $this->twig->display('admin/employee/update', $data);
    }

    public function delete($id)
    {
        $this->ion_auth->delete_user($id);
        redirect(route('admin.employee'), 'refresh');
    }

    public function store()
    {      
        $form_type = $this->input->post('form_type');   
        $this->form_validation->set_rules($this->_configValidation($form_type));       
        $dataForm = (object) $this->input->post();
        if ($this->form_validation->run()) 
        {
            if ($form_type == 'create') {
                $this->_insert($dataForm);
            } else {
                $this->_set($dataForm);
            }    
        }
        else
        {
            $form_type == 'create' ? $this->create() : $this->update($dataForm->id);
        }        
    }

    private function _insert($dataForm)
    {
        $email = $dataForm->email;
        $password = $dataForm->password;
        $additional_data = array(
            'name' => $dataForm->name,
        );
        $this->ion_auth->register($email, $password, $email, $additional_data); 
        redirect(route('admin.employee'), 'refresh');
    }

    private function _set($dataForm)
    {
        $id = $dataForm->id;
        $data['email'] = $dataForm->email;
        $data['name'] = $dataForm->name;
        if ($dataForm->password != '') 
            $data['password'] = $dataForm->password;
        
        $this->ion_auth->update($id, $data);
        redirect(route('admin.employee'), 'refresh');
    }

    private function _configValidation($form_type)
    {
        $config = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required'
            ),array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required'
            ),
            array(
                'field' => 'name',
                'label' => 'Nama Lengkap',
                'rules' => 'required'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => ($form_type == 'create' ? 'required' : ''),
                'errors' => array(
                    'required' => 'You must provide a %s.',
                ),
            ),
        );
        return $config;
    }

    function seed()
    {        
        include APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        foreach (range(0, 10) as $number) {
            $employee = new Employee;
            $employee->ip_address = 'pegawai';
            $employee->username = $faker->unique()->userName;
            $employee->email = $faker->email;
            $employee->name = $faker->name;
            $employee->password = 'rahasia';
            $employee->active = 1;
            $employee->save();
        }

        echo 'Seeding done!';
    }

}