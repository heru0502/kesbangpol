<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ApplicantController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Applicant');

        /* Set Groups Allowed */
        $this->middlewarefilter->filter('AdminMiddleware', ['admin']);
    }

    
    /**
     * Index action
     */
    public function index()
    {
        $this->twig->display('admin/applicant/index');
    }

}