<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ApplicantReportController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Applicant');

        /* Set Groups Allowed */
        $this->middlewarefilter->filter('AdminMiddleware', ['admin','employee']);
    }

    
    /**
     * Index action
     */
    public function index()
    {
        $this->twig->display('admin/applicant_report/index');
    }

    public function printApplicantReport()
    {
        $this->load->library('PdfGenerator');
        $dateRange = $this->input->post('reservation');
        $dateRangeExplode = explode(' - ', $dateRange);
        $from = date('Y-m-d', strtotime(str_replace('/', '-', $dateRangeExplode[0])));
        $now = date('Y-m-d', strtotime(str_replace('/', '-', $dateRangeExplode[1]) . ' +1 day'));

        $applicant = new Applicant;
        $data['current_data'] = $applicant->index()->whereBetween('created_at', [$from, $now])->get();
        $data['date_renge'] = $dateRange;
        $html = $this->twig->render('admin/applicant_report/print', $data, true);
		$fileName = 'Laporan_Peserta';			
		$this->pdfgenerator->generate($html, $fileName, 'A4', 'portrait');
    }

}