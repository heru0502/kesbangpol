<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DashboardController extends CI_Controller {

	public function __construct()
	{
        parent::__construct();          
        $this->load->model('admin/Submission');

        /* Set Groups Allowed */
        $this->middlewarefilter->filter('AdminMiddleware', ['admin','employee']);
    }

    function index()
    {
        $data['sum_submission_entry'] = Submission::where('submission_status', '!=', 0)->count();
        $data['sum_submission_waiting'] = Submission::where('submission_status', 1)->count();
        $data['sum_submission_done'] = Submission::where('submission_status', 3)->count();
        $data['sum_applicant'] = count($this->ion_auth->users('applicant')->result());
        $this->twig->display('admin/dashboard/index', $data);
    }
      
}