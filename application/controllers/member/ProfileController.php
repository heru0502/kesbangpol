<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('member/User');
        $this->load->model('member/AdditionalDataApplicant');
        $this->user = $this->ion_auth->user()->row();
        $this->datetimeNow = date('YmdHis');
        $this->pictureName = null;

        /* Set Groups Allowed */
        $this->middlewarefilter->filter('MemberMiddleware', ['applicant']);
    }

    
    /**
     * Index action
     */
    public function index()
    {
        $data['user'] = $this->user;
        $data['current_data'] = User::with('additionalData')->find($this->user->id)->toArray();
        $this->twig->display('member/profile/index', $data);
    }

    public function store()
    {
        $this->form_validation->set_rules($this->_configValidation());         
        $dataForm = (object) $this->input->post();

        if ($this->form_validation->run()) 
        {
            $user = User::find($this->user->id);
            $user->username = $dataForm->email;
            $user->email = $dataForm->email;
            $user->name = $dataForm->name;
            if (!empty($_FILES['picture']['name']))
                $user->picture = $this->pictureName;
            if ($dataForm->password != '') 
                $user->password = password_hash($dataForm->password, PASSWORD_BCRYPT);
            $user->save();

            $additional = AdditionalDataApplicant::find($this->user->id);
            $additional->identity_number = $dataForm->identity_number;
            $additional->address = $dataForm->address;
            $additional->institution = $dataForm->institution;
            $additional->faculty = $dataForm->faculty;
            $additional->save();
            redirect(route('member.profile'), 'refresh');
        }
        else
        {
            $this->index();
        }
    }

    private function _configValidation()
    {
        $config = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required'
            ),
            array(
                'field' => 'identity_number',
                'label' => 'Nomor Identitas',
                'rules' => 'required'
            ),
            array(
                'field' => 'name',
                'label' => 'Nama / Obyek',
                'rules' => 'required'
            ),
            array(
                'field' => 'address',
                'label' => 'Alamat',
                'rules' => 'required'
            ),
            array(
                'field' => 'institution',
                'label' => 'Lembaga Pendidikan / Perguruan Tinggi',
                'rules' => 'required',
            ),
            array(
                'field' => 'faculty',
                'label' => 'Fakultas / Jurusan',
                'rules' => 'required'
            ),
            array(
                'field' => 'picture',
                'rules' => (!empty($_FILES['picture']['name']) ? 'callback__rule_attachment' : ''),
            ),
        );
        return $config;
    }

    function _rule_attachment()
	{
		$config['upload_path']          = './uploads/profiles';
		$config['file_name']            = $this->datetimeNow;
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 5000;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('picture')){
			$messageError = $this->upload->display_errors();
            $this->form_validation->set_message('_rule_attachment', $messageError);
			return false;
		}
		else{		
            $upload_data = $this->upload->data(); 
            $this->pictureName = $upload_data['file_name'];
			return true;
		}
	}

}