<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->data['title'] = 'Beranda';  
        // $this->load->model('member/Home');

        /* Set Groups Allowed */
        // $this->middlewarefilter->filter('MemberMiddleware', ['applicant']);
    }

    
    /**
     * Index action
     */
    public function index()
    {
        $data = $this->data;
        $this->twig->display('member/home/index', $data);
    }

}