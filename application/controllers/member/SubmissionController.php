<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SubmissionController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();        
        $this->load->model('member/Submission');
        $this->load->model('member/User');
        $this->load->model('member/AdditionalDataApplicant');
        $this->load->model('Info');
        $this->user = $this->ion_auth->user()->row();
        $this->datetimeNow = date('YmdHis');

        /* Set Groups Allowed */
        $this->middlewarefilter->filter('MemberMiddleware', ['applicant']);
    }

    
    /**
     * Index action
     */
    public function index()
    {
        $data['user'] = $this->user;
        $this->twig->display('member/submission/index', $data);
    }

    public function create()
    {        
        $data['user'] = $this->user;
        $data['current_data'] = User::with('additionalData')->find($this->user->id)->toArray();
        $data['form_type'] = 'create';
        $data['info_attachment'] = Info::find('attachment')->toArray();
        $this->twig->display('member/submission/create', $data);
    }

    public function update($id)
    {
        $data['user'] = $this->user;
        $data['current_data'] = Submission::find($id)->toArray();
        $data['form_type'] = 'update';
        $this->twig->display('member/submission/update', $data);
    }

    public function delete($id)
    {
        Submission::destroy($id);
        redirect(route('member.submission'), 'refresh');
    }

    public function store()
    {      
        $form_type = $this->input->post('form_type'); 
        $this->form_validation->set_rules($this->_configValidation($form_type));         
        $dataForm = (object) $this->input->post();

        if ($this->form_validation->run()) 
        {
            if ($form_type == 'create') 
            {
                $submission = new Submission;
                $submission->created_by = $this->user->id;
                $submission->attachment = $this->datetimeNow . '.zip';
            } 
            else 
            {
                $submission = Submission::find($dataForm->id);
                $submission->updated_by = $this->user->id;
            }    

            $submission->submission_status = ($dataForm->buttonSubmit == 'saveAndSend') ? 1 : 0;
            $submission->identity_number = $dataForm->identity_number;
            $submission->name = $dataForm->name;
            $submission->address = $dataForm->address;
            $submission->institution = $dataForm->institution;
            $submission->faculty = $dataForm->faculty;
            $submission->research_title = $dataForm->research_title;
            $submission->research_location = $dataForm->research_location;
            $submission->research_time_start = date('Y-m-d', strtotime("01-$dataForm->research_time_start"));
            $submission->research_time_end = date('Y-m-d', strtotime("01-$dataForm->research_time_end"));
            $submission->research_category = $dataForm->research_category;
            $submission->research_status = $dataForm->research_status;
            $submission->research_member = $dataForm->research_member;
            $submission->tembusan = $dataForm->tembusan;
            $submission->save();
            redirect(route('member.submission'), 'refresh');
        }
        else
        {
            $form_type == 'create' ? $this->create() : $this->update($dataForm->id);
        }    
    }

    private function _configValidation($form_type)
    {
        $config = array(
            array(
                'field' => 'identity_number',
                'label' => 'Nomor Identitas',
                'rules' => 'required'
            ),array(
                'field' => 'name',
                'label' => 'Nama / Obyek',
                'rules' => 'required'
            ),
            array(
                'field' => 'address',
                'label' => 'Alamat',
                'rules' => 'required'
            ),
            array(
                'field' => 'institution',
                'label' => 'Lembaga Pendidikan / Perguruan Tinggi',
                'rules' => 'required',
            ),
            array(
                'field' => 'faculty',
                'label' => 'Fakultas / Jurusan',
                'rules' => 'required'
            ),
            array(
                'field' => 'research_title',
                'label' => 'Judul Penelitian',
                'rules' => 'required'
            ),
            array(
                'field' => 'research_location',
                'label' => 'Alamat',
                'rules' => 'required'
            ),
            array(
                'field' => 'research_time_start',
                'label' => 'Waktu / Lama Penelitian',
                'rules' => 'required'
            ),
            array(
                'field' => 'research_time_end',
                'label' => 'Waktu / Lama Penelitian',
                'rules' => 'required'
            ),
            array(
                'field' => 'research_category',
                'label' => 'Bidang Penelitian',
                'rules' => 'required'
            ),
            array(
                'field' => 'research_status',
                'label' => 'Status Penelitian',
                'rules' => 'required'
            ),
            array(
                'field' => 'attachment',
                'rules' => ($form_type == 'create' ? 'callback__rule_attachment' : ''),
            ),
        );
        return $config;
    }

    function _rule_attachment()
	{
		$config['upload_path']          = './uploads/attachments';
		$config['file_name']            = $this->datetimeNow;
		$config['allowed_types']        = 'zip|rar';
		$config['max_size']             = 7000;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('attachment')){
			$messageError = $this->upload->display_errors();
            $this->form_validation->set_message('_rule_attachment', $messageError);
			return false;
		}
		else{			
			return true;
		}
    }

}