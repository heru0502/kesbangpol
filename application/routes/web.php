<?php

/**
 * Welcome to Luthier-CI!
 *
 * This is your main route file. Put all your HTTP-Based routes here using the static
 * Route class methods
 *
 * Examples:
 *
 *    Route::get('foo', 'bar@baz');
 *      -> $route['foo']['GET'] = 'bar/baz';
 *
 *    Route::post('bar', 'baz@fobie', [ 'namespace' => 'cats' ]);
 *      -> $route['bar']['POST'] = 'cats/baz/foobie';
 *
 *    Route::get('blog/{slug}', 'blog@post');
 *      -> $route['blog/(:any)'] = 'blog/post/$1'
 */

Route::get('/', function(){
   redirect(route('member.login'), 'refresh');
})->name('home');

Route::get('/admin-page', function(){
    redirect(route('admin.login'), 'refresh');
 })->name('admin.home');

Route::set('404_override', function(){
    show_404();
});

Route::set('translate_uri_dashes',FALSE);

/* Admin */

Route::group('admin', function() {    
    Route::get('login', 'admin/LoginController@index')->name('admin.login');
    Route::post('login/verify', 'admin/AuthController@login')->name('admin.login.verify');
    Route::get('logout', 'admin/AuthController@logout')->name('admin.logout');

    Route::get('dashboard', 'admin/DashboardController@index')->name('admin.dashboard');

    Route::group('employee', function() {
        Route::get('/', 'admin/EmployeeController@index')->name('admin.employee');
        Route::get('create', 'admin/EmployeeController@create')->name('admin.employee.create');
        Route::get('update/{id}', 'admin/EmployeeController@update')->name('admin.employee.update');
        Route::get('delete/{id}', 'admin/EmployeeController@delete')->name('admin.employee.delete');
        Route::post('store', 'admin/EmployeeController@store')->name('admin.employee.store');
    });  
    
    Route::group('applicant', function() {
        Route::get('/', 'admin/ApplicantController@index')->name('admin.applicant');
        Route::get('create', 'admin/ApplicantController@create')->name('admin.applicant.create');
        Route::get('update/{id}', 'admin/ApplicantController@update')->name('admin.applicant.update');
        Route::get('delete/{id}', 'admin/ApplicantController@delete')->name('admin.applicant.delete');
        Route::post('store', 'admin/ApplicantController@store')->name('admin.applicant.store');
    });  

    Route::group('submission', function() {
        Route::get('/', 'admin/SubmissionController@index')->name('admin.submission');
        Route::get('create', 'admin/SubmissionController@create')->name('admin.submission.create');
        Route::get('update/{id}', 'admin/SubmissionController@update')->name('admin.submission.update');
        Route::get('update/revision/{id}', 'admin/SubmissionController@revision')->name('admin.submission.update.revision');
        Route::get('update/numbering/{id}', 'admin/SubmissionController@numbering')->name('admin.submission.update.numbering');
        Route::get('print/{id}', 'admin/SubmissionController@printSubmission')->name('admin.submission.print');
        Route::get('delete/{id}', 'admin/SubmissionController@delete')->name('admin.submission.delete');
        Route::post('store', 'admin/SubmissionController@store')->name('admin.submission.store');
    });        

    Route::group('report', function() {
        Route::get('applicant', 'admin/ApplicantReportController@index')->name('admin.report.applicant');
        Route::post('applicant/print', 'admin/ApplicantReportController@printApplicantReport')->name('admin.report.applicant.print');
        
        Route::get('submission', 'admin/SubmissionReportController@index')->name('admin.report.submission');
        Route::post('submission/print', 'admin/SubmissionReportController@printSubmissionReport')->name('admin.report.submission.print');
    });     
    
    Route::get('profile', 'admin/ProfileController@index')->name('admin.profile');
    Route::post('profile/store', 'admin/ProfileController@store')->name('admin.profile.store');
});

/* Member */
Route::group('member', function() {
    Route::get('login', 'member/AuthController@login')->name('member.login');
    Route::post('login/verify', 'member/AuthController@loginVerify')->name('member.login.verify');
    Route::get('logout', 'member/AuthController@logout')->name('member.logout');
    Route::get('register', 'member/AuthController@register')->name('member.register');
    Route::post('register/verify', 'member/AuthController@create_user')->name('member.register.verify');
    Route::get('activate/{id}/{code}', 'member/AuthController@activate')->name('member.activate');
    Route::get('forgot-password', 'member/AuthController@forgotPassword')->name('member.forgotPassword');
    Route::post('forgot-password/verify', 'member/AuthController@forgotPasswordVerify')->name('member.forgotPassword.verify');
    Route::get('reset-password/{code}', 'member/AuthController@reset_password')->name('member.resetPassword');
    Route::post('reset-password/verify/{code}', 'member/AuthController@reset_password')->name('member.resetPassword.verify');

    Route::get('submission', 'member/SubmissionController@index')->name('member.submission');
    Route::get('submission/create', 'member/SubmissionController@create')->name('member.submission.create');
    Route::get('submission/update/{id}', 'member/SubmissionController@update')->name('member.submission.update');
    Route::get('submission/delete/{id}', 'member/SubmissionController@delete')->name('member.submission.delete');
    Route::post('submission/store', 'member/SubmissionController@store')->name('member.submission.store');

    Route::get('profile', 'member/ProfileController@index')->name('member.profile');
    Route::post('profile/store', 'member/ProfileController@store')->name('member.profile.store');
    // Route::get('home', 'member/HomeController@index')->name('member.home');
});

