<?php

/**
 * API Routes
 *
 * This routes only will be available under AJAX requests. This is ideal to build APIs.
 */

Route::post('function/datatable', 'Functions/DataTableController@generateDataIndex');
// Route::get('function/test', 'Functions/DataTableController@test');