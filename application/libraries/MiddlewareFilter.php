<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Luthier\Middleware;

class MiddlewareFilter
{
    public function filter($middleware, $args = '') {
        $middlewareFunction = new Middleware();
        $middlewareFunction->run($middleware, $args);
    }
}