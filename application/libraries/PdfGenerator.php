<?php
/*
|
|       SatrioDwiPrabowo Was Here !
|       Apps name       : SisPeKK
|       Developer       : SatrioDwiPrabowo@ITDept
|       Company         : PT Plasindo Lestari, Karawang, Indonesia
|
*/
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("./vendor/dompdf/dompdf/src/Autoloader.php");
use Dompdf\Dompdf;

class PdfGenerator 
{
    // public function generate($html, $filename='[PL]Laporan_Nilai', $stream=TRUE, $paper = 'A4', $orientation = "landscape")
    // {
    //     $dompdf = new Dompdf(array('enable_remote' => true, 'isHtml5ParserEnabled' => true));   
    //     $dompdf->loadHtml($html);
    //     $dompdf->setPaper(array(0, 0, 580, 794), $orientation);
    //     $dompdf->render();
    //     if ($stream) {
    //         $dompdf->stream($filename.".pdf", array("Attachment" => 0));
    //     } else {
    //         return $dompdf->output();
    //     }
    // }

    public function generate($html, $filename, $paper, $orientation, $stream=TRUE)
    {
        $dompdf = new Dompdf(array('enable_remote' => true, 'isHtml5ParserEnabled' => true));    
        $dompdf->loadHtml($html);
        $dompdf->setPaper($paper, $orientation);
        $dompdf->render();
        if ($stream) {
            $dompdf->stream($filename.".pdf", array("Attachment" => 0));
        } else {
            return $dompdf->output();
        }
    }
}