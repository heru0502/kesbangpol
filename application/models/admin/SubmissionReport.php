<?php
use \Illuminate\Database\Eloquent\Model as Eloquent;

class SubmissionReport extends Eloquent
{
    protected $table = 'submissions';
    // public $timestamps = false;

    public function index() {
        $ci = get_instance();
        $user = $ci->ion_auth->user()->row();
        return SubmissionReport::where('submission_status', 3);
    }
}