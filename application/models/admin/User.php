<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent
{
    protected $table = 'users';
    public $timestamps = false;

    public function userGroup()
    {        
        return $this->belongsToMany('UserGroup')->withPivot('user_id', 'group_id');
    }

    // public function group()
    // {        
    //     return $this->hasOne('Group');
    // }
}