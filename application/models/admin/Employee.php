<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Employee extends Eloquent
{
    protected $table = 'users';
    // public $timestamps = false;

    public function index() {
        $ci = get_instance();
        $user = $ci->ion_auth->user()->row();
        return Employee::select('users.*')
                        ->whereIn('groups.name', array('admin','employee'))
                        ->join('users_groups', 'users_groups.user_id', '=', 'users.id')
                        ->join('groups', 'groups.id', '=', 'users_groups.group_id')
                        ->groupBy('email');
    }

    
}