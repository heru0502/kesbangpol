<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Applicant extends Eloquent
{
    protected $table = 'users';
    // public $timestamps = false;

    public function index() {
        $ci = get_instance();
        $user = $ci->ion_auth->user()->row();
        return Applicant::select('users.*', 'identity_number', 'address', 'institution', 'faculty')
                        ->whereIn('groups.name', array('applicant'))
                        ->join('additional_data_applicants', 'additional_data_applicants.user_id', 'users.id')
                        ->join('users_groups', 'users_groups.user_id', '=', 'users.id')
                        ->join('groups', 'groups.id', '=', 'users_groups.group_id');
                        // ->groupBy('email');
    }

    
}