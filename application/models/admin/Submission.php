<?php
use \Illuminate\Database\Eloquent\Model as Eloquent;

class Submission extends Eloquent
{
    protected $table = 'submissions';
    // public $timestamps = false;

    public function index() {
        $ci = get_instance();
        $user = $ci->ion_auth->user()->row();
        return Submission::where('submission_status', '!=', 0);
    }
}