<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Auth extends Eloquent
{
    protected $table = 'users';
    public $timestamps = false;

}