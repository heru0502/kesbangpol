<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class AdditionalDataApplicant extends Eloquent
{
    protected $table = 'additional_data_applicants';
    protected $primaryKey = 'user_id';
    public $timestamps = false;    
}