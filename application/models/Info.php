<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Info extends Eloquent
{
    protected $table = 'infos';
    public $timestamps = false;    
}